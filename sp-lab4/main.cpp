﻿#include <Windows.h>
#include <windowsx.h>
#include "resource.h"

const int ID_TIMER = 1;

struct Animation
{
    int x = 0;
    int y = 0;

    int dx = 1;
    int dy = 1;

    bool is_moving = false;

    POINT destination = { 0, 0 };

    INT8 currentFrame = -1;
    INT8 nextFrame = 1;

    HBITMAP frames[4] = {
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP1)),
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP2)),
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP3)),
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP4)),
    };

    HBITMAP framesReversed[4] = {
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP5)),
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP6)),
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP7)),
        LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP8)),
    };

    HBITMAP getCurrentFrame()
    {
        currentFrame += nextFrame;
        if ((currentFrame < 0) || (currentFrame > 3))
        {
            nextFrame *= -1;
            currentFrame += nextFrame * 2;
        }

        return (dx > 0) ? frames[currentFrame] : framesReversed[currentFrame];
    }

    void changeSpeed()
    {
        dx = (x < destination.x) ? 1 : -1;
        dy = (y < destination.y) ? 1 : -1;
    }

    void move()
    {
        if (x != destination.x)
            x += dx;
        if (y != destination.y)
            y += dy;
        if ((x == destination.x) && (y == destination.y))
            is_moving = false;
    }

} animation;

LPCWSTR windowClassName = L"sp_lab4";

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

void DrawBitmap(HDC hdc, HBITMAP hBitmap, int xStart, int yStart);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX wc;
    HWND hWnd;
    MSG Msg;

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 1;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
    wc.lpszMenuName = nullptr;
    wc.lpszClassName = windowClassName;
    wc.hIconSm = nullptr;

    if (!RegisterClassEx(&wc))
    {
        MessageBox(nullptr, L"Couldn't register the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    hWnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        windowClassName,
        L"Лабораторная работа №4",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, 600, 400,
        nullptr, nullptr, hInstance, nullptr
    );

    if (hWnd == nullptr)
    {
        MessageBox(nullptr, L"Couldn't create the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    while (GetMessage(&Msg, nullptr, 0, 0) > 0)
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    return Msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch (Msg)
    {
        case WM_CREATE:
            if (!SetTimer(hWnd, ID_TIMER, 50, nullptr))
            {
                MessageBox(hWnd, L"Couldn't create a timer.", L"Error!",
                    MB_ICONERROR | MB_OK);
                return FALSE;
            }
            break;
        case WM_TIMER:
        {
            if (animation.is_moving)
            {
                animation.move();
                RECT rect;
                GetClientRect(hWnd, &rect);
                InvalidateRect(hWnd, &rect, TRUE);
            }
            break;
        }
        case WM_PAINT:
        {
            HDC hdc;
            PAINTSTRUCT ps;

            hdc = BeginPaint(hWnd, &ps);
            DrawBitmap(hdc, animation.getCurrentFrame(), animation.x, animation.y);
            EndPaint(hWnd, &ps);
            break;
        }
        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* mmi = reinterpret_cast<MINMAXINFO*> (lParam);
            mmi->ptMinTrackSize.x = 600;
            mmi->ptMinTrackSize.y = 400;
            break;
        }
        case WM_LBUTTONUP:
            animation.is_moving = true;
            animation.destination.x = GET_X_LPARAM(lParam);
            animation.destination.y = GET_Y_LPARAM(lParam);
            animation.changeSpeed();
            break;
        case WM_CLOSE:
            DestroyWindow(hWnd);
            break;
        case WM_DESTROY:
            KillTimer(hWnd, ID_TIMER);
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hWnd, Msg, wParam, lParam);
    }

    return 0;
}

void DrawBitmap(HDC hdc, HBITMAP hBitmap, int xStart, int yStart)
{
    HDC hdcMem = CreateCompatibleDC(hdc);

    SelectObject(hdcMem, hBitmap);
    SetMapMode(hdcMem, GetMapMode(hdc));

    BITMAP bitmap;

    GetObject(hBitmap, sizeof(BITMAP), static_cast<LPVOID>(&bitmap));

    POINT ptSize = { bitmap.bmWidth, bitmap.bmHeight };
    DPtoLP(hdcMem, &ptSize, 1);

    POINT ptOrg = { 0, 0 };
    DPtoLP(hdcMem, &ptOrg, 1);

    BitBlt(
        hdc, xStart, yStart, ptSize.x, ptSize.y,
        hdcMem, ptOrg.x, ptOrg.y, SRCCOPY
    );

    DeleteDC(hdcMem);
}
